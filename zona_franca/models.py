from django.db import models


class Logistica(models.Model):
    estado = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.estado


class Acondicionamiento(models.Model):
    estado = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.estado


class Zona(models.Model):
    estado = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.estado


class CategoriaProducto(models.Model):
    nombre = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre


class Destruccion(models.Model):
    estado = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.estado


class Exportacion(models.Model):
    estado = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.estado


class Almacenamiento(models.Model):
    estado = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.estado


class Producto(models.Model):
    nombre = models.CharField(max_length=100)
    cantidad = models.CharField(max_length=100)
    categoria = models.ForeignKey(CategoriaProducto, on_delete=True, null=True, blank=True)
    logistica = models.ForeignKey(Logistica, on_delete=True, null=True, blank=True)
    zona = models.ForeignKey(Zona, on_delete=True, null=True, blank=True)
    acondicionamiento = models.ForeignKey(Acondicionamiento, on_delete=True, null=True, blank=True)
    exportacion = models.ForeignKey(Exportacion, on_delete=True, null=True, blank=True)
    destruccion = models.ForeignKey(Destruccion, on_delete=True, null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return self.nombre
