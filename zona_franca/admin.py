from django.contrib import admin
from zona_franca.models import *


class LogisticaAdmin(admin.ModelAdmin):
    pass


class DestruccionAdmin(admin.ModelAdmin):
    pass


class ExportacionAdmin(admin.ModelAdmin):
    pass


class AlmacenamientoAdmin(admin.ModelAdmin):
    pass


class AcondicionamientoAdmin(admin.ModelAdmin):
    pass


class ZonaAdmin(admin.ModelAdmin):
    pass


class ProductoAdmin(admin.ModelAdmin):
    model = Producto
    list_display = (
            'nombre',
            'cantidad',
            'categoria',
            'logistica',
            'zona',
            'acondicionamiento',
            'exportacion',
            'destruccion',
            'created_at',
    )


class CategoriaAdmin(admin.ModelAdmin):
    pass


admin.site.register(Logistica, LogisticaAdmin)
admin.site.register(Destruccion, DestruccionAdmin)
admin.site.register(Exportacion, ExportacionAdmin)
admin.site.register(Almacenamiento, AlmacenamientoAdmin)
admin.site.register(Acondicionamiento, AcondicionamientoAdmin)
admin.site.register(Zona, ZonaAdmin)
admin.site.register(CategoriaProducto, CategoriaAdmin)
admin.site.register(Producto, ProductoAdmin)
