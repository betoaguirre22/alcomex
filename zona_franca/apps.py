from django.apps import AppConfig


class ZonaFrancaConfig(AppConfig):
    name = 'zona_franca'
    verbose_name = "Administrador de procesos  Alcodex"
